import './App.css';
import Carousel from './components/common/carousel/Carousel.jsx';

const App = () => {
    return (
        <div className="App">
            <header className="App-header">
				<Carousel slides={1} infinite={false}/>
        		<Carousel slides={4} infinite={true}/>
        		<Carousel slides={10} infinite={false}/>
            </header>
        </div>
  	);
}

export default App;

import { useState, useEffect } from 'react';
import axios from 'axios';
import { Empty } from 'antd';
import { LeftCircleOutlined, RightCircleOutlined } from '@ant-design/icons';

import './styles.css';

const Carousel = (props) => {
    const [imageList, setImageList] = useState(null);
    const [current, setCurrent] = useState(0);
    const length = props.slides;

    useEffect(() => {
        const fetchData = async (slides) => {
            console.log(slides);
            try {
                const { data, status } = await axios.get(`http://localhost:3600/images/${slides}`);
                const responseData = data.data;
                if (!data.error && responseData && status === 200) {
                    setImageList(responseData || []);
                } else if (status === 204) {
                    setImageList([]);
                } else {
                    setImageList([]);
                    console.log("Error from API. Please check again");
                }
            } catch (ex) {}
        };

        if (props.slides && !isNaN(props.slides)) {
            fetchData(props.slides);
        }
        
    }, [props.slides]);

    const nextSlide = () => {
        if (current === length-1) {
            setCurrent(props.infinite ? 0 : current);
        } else {
            setCurrent(current+1);
        }
    };
    
    const previousSlide = () => {
        if (current === 0) {
            setCurrent(props.infinite ? length-1 : current);
        } else {
            setCurrent(current-1);
        }
    };

    return (
        <div className="container">
            {imageList ? (
                <>
                    {props.slides > 1 ? (
                            <>
                                <div className="leftArrow" onClick={previousSlide}>
                                    <LeftCircleOutlined />
                                </div>
                                <div className="rightArrow" onClick={nextSlide}>
                                    <RightCircleOutlined />
                                </div>
                            </>
                        ) : (
                            <></>
                        )
                    }
                    <div className="title">{imageList[current].title}</div>
                    <div className="subtitle">{imageList[current].subtitle}</div>
                    <img src={imageList[current].image} alt="display slide" className="carouselImage" />
                </>
            ) : (
                <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
            )}
        </div>
    );
}

export default Carousel;